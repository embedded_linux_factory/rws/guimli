#ifndef PIN_H
#define PIN_H

#include <nlohmann/json.hpp>
using json = nlohmann::json;

class Pin
{
public:
    Pin(int off, bool dir,int val,
        bool status, std::string ur);

    /*!
        \brief encode all the Pin information as json format.
    */
    json encodeToJson();
    int offset;
    int direction;
    int value;
    int used;
    std::string user;

private:


};

#endif // PIN_H
