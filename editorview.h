#ifndef EDITORVIEW_H
#define EDITORVIEW_H

#include <QWidget>
#include "QDropEvent"
#include <QTreeView>


class EditorView : public QTreeView
{
    Q_OBJECT
public:
    explicit EditorView(QWidget *parent = nullptr);


protected:
    void dropEvent(QDropEvent* event);


signals:


};

#endif // EDITORVIEW_H
