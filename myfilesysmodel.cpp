#include "myfilesysmodel.h"
#include <iostream>

MyFileSysModel::MyFileSysModel(QWidget *parent) : QFileSystemModel(parent)
{

}


// MyFileSysModel is a child from model class used in your example.
// Mind that specific application drag and drop logic may differ.
// I in fact modified that from QSortFilterProxyModel-type of class
// but that should be similar.
Qt::ItemFlags MyFileSysModel::flags(const QModelIndex &index) const
{


    Qt::ItemFlags defaultFlags = QFileSystemModel::flags(index);

    if (!index.isValid())
        return defaultFlags;

    const QFileInfo& fileInfo = this->fileInfo(index);

    // The target
    if (fileInfo.isDir())
    {
        // allowed drop
        return Qt::ItemIsDropEnabled | Qt::ItemIsDragEnabled | defaultFlags;
    }
    // The source: should be directory (in that case)
    else if (fileInfo.isFile())
    {
        // allowed drag
        return Qt::ItemIsDropEnabled |Qt::ItemIsDragEnabled | defaultFlags;
    }

    return defaultFlags;
}


Qt::DropActions MyFileSysModel::supportedDropActions() const
{
    return Qt::CopyAction | Qt::MoveAction;
}


