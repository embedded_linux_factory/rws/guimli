#include "chipset.h"

using json = nlohmann::json;

#include <iostream>
#include <QTreeWidgetItem>
Chipset::Chipset(std::string chipset_name)
{
    name = chipset_name;

}


void Chipset::addPin(Pin * pin){
    pins.push_back(pin);

}

void Chipset::showPins(QTreeWidget * tree){
    //Filling the TreeWidget in the main Window
    for (auto pin: pins){
        QTreeWidgetItem * pinItem = new QTreeWidgetItem(tree);
        pinItem->setText(0, QString::number(pin->offset));
        pinItem->setText(1, QString::number(pin->direction));
        pinItem->setText(2, QString::number(pin->value));
        pinItem->setText(3, QString::number(pin->used));
        pinItem->setText(4, QString::fromStdString(pin->user));

    }
}

json Chipset::encodeToJson(){
    json jsonMessage ;

    for (Pin * pin : pins){
        jsonMessage[name]+= pin->encodeToJson();
    }

    std::cout << jsonMessage << std::endl;

    std::cout << jsonMessage.dump(5) << std::endl;
    return jsonMessage;

}
