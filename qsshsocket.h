#ifndef QSSHSOCKET_H
#define QSSHSOCKET_H

#include <libssh/libssh.h>
#include <QByteArray>
#include <QThread>
#include <QVector>
#include <QFile>
#include <sys/stat.h>
#include <sys/types.h>
#include <QDebug>
#include <libssh/sftp.h>
#include <fcntl.h>

#define MAX_XFER_BUF_SIZE 16384

class QSshSocket: public QThread
{
    Q_OBJECT
public:

    enum SshError
    {
        /*! \brief There was trouble creating a socket. This was most likely due to the lack of an internet connection.*/
        SocketError,
        /*! \brief The ssh session could not be created due to inability to find the remote host.*/
        SessionCreationError,
        /*! \brief An ssh channel could not be created for the previous operation.*/
        ChannelCreationError,
        /*! \brief An scp channel could not be created for the previous file transfer operation.*/
        ScpChannelCreationError,
        /*! \brief There was an error requesting a pull file transfer.*/
        ScpPullRequestError,
        /*! \brief There was an error requesting a push file transfer.*/
        ScpPushRequestError,
        /*! \brief The destination file for the previous transfer does not exist.*/
        ScpFileNotCreatedError,
        /*! \brief There was an error reading a remote file. This could possibly be due to user permissions.*/
        ScpReadError,
        /*! \brief There was an error writing to a remote file. This could possibly be due to user permissions.*/
        ScpWriteError,
        /*! \brief The credentials of a user on the remote host could not be authenticated.*/
        PasswordAuthenticationFailedError
    };

    /*!
        \param position The center position of the box.
        \param size The size of the box.
        \brief The constructor.
    */
    explicit QSshSocket(QObject * parent = 0);

    /*!
        \brief The deconstructor.
    */
    ~QSshSocket();

    /*!
        \param host The hostname to establish an ssh connection with.
        \param port The port to establish an ssh connection over.
        \brief This function connects this socket to the specified host over the specified port. On success, the signal connected is emitted while error is emmited on failure.
    */
    void connectToHost(QString host, int port =22);

    /*!
        \brief This function disconnects the socket from the current host (if there is one. On success, the signal disconnected is emitted while error is emmited on failure.
    */
    void disconnectFromHost();


    /*!
        \brief Returns the hostname of the remote machine this socket is connected to. If not connected to a remote host, this returns "".
    */
    QString host();

    /*!
        \brief Returns whether or not a user has been logged in at remote host.
    */
    bool isLoggedIn();

    /*!
        \brief Returns whether or not this socket is currently connected to a remote host.
    */
    bool isConnected();

    /*!
        \param user The username to login with.
        \param password The password of the account for the specified username.
        \brief This function to login to the currently connected host given credentials.
        On success, the signal authenticated is emitted while error is emmited on failure.
    */
    void login(QString user, QString password);

    /*!
        \brief Returns the port of the current connection. If not connected to a remote host, this returns -1.
    */
    int port();


    /*!
        \brief Returns the username of the current authenticated user on the remote host. If not connected to a remote host, or if a user has not been authenticated this returns "".
    */
    QString user();

    int verify_knownhost(ssh_session session);

    //int connectSftp();



signals:

    /*!
        \brief This signal is emitted when remote host has been connected to."
    */
    void connected();

    /*!
        \brief This signal is emitted when this class has been properly disconnected from a remote host.
    */
    void disconnected();

    /*!
        \param error The type of error that occured.
        \brief This signal is emitted when an error occurs.
    */
    void error(QSshSocket::SshError error);



    /*!
        \brief This signal is emitted when a user has been loggen in to the remote host."
    */
    void loginSuccessful();






private slots:
    void run();

private:

    int m_port;
    bool m_loggedIn ;
    QThread * m_thread;
    QString m_user, m_host,m_password;

    ssh_session m_session;
    bool m_connected,m_run;
    sftp_session m_sftp;
};


#endif // QSSHSOCKET_H
