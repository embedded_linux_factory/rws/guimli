/********************************************************************************
** Form generated from reading UI file 'parametersdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PARAMETERSDIALOG_H
#define UI_PARAMETERSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_parametersDialog
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_8;
    QHBoxLayout *horizontalLayout_5;
    QLabel *IP_label;
    QLineEdit *IPLineEdit;
    QHBoxLayout *horizontalLayout_6;
    QLabel *User_label;
    QLineEdit *userLineEdit;
    QHBoxLayout *horizontalLayout_7;
    QLabel *Port_label_2;
    QLineEdit *portLineEdit;
    QWidget *layoutWidget_2;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_7;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_5;
    QLineEdit *baudrateLineEdit;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_6;
    QLineEdit *serialPortLineEdit;
    QComboBox *profilesBox;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout;
    QPushButton *LoadProfileButton;
    QPushButton *saveProfileButton;
    QPushButton *removeProfileButton;
    QPushButton *saveAsDefaultButton;

    void setupUi(QDialog *parametersDialog)
    {
        if (parametersDialog->objectName().isEmpty())
            parametersDialog->setObjectName(QString::fromUtf8("parametersDialog"));
        parametersDialog->resize(559, 359);
        buttonBox = new QDialogButtonBox(parametersDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(360, 310, 171, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        layoutWidget = new QWidget(parametersDialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 10, 239, 156));
        verticalLayout_3 = new QVBoxLayout(layoutWidget);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        QFont font;
        font.setPointSize(16);
        font.setItalic(true);
        label_8->setFont(font);

        verticalLayout_3->addWidget(label_8);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        IP_label = new QLabel(layoutWidget);
        IP_label->setObjectName(QString::fromUtf8("IP_label"));

        horizontalLayout_5->addWidget(IP_label);

        IPLineEdit = new QLineEdit(layoutWidget);
        IPLineEdit->setObjectName(QString::fromUtf8("IPLineEdit"));
        IPLineEdit->setMinimumSize(QSize(190, 25));
        IPLineEdit->setMaximumSize(QSize(190, 25));

        horizontalLayout_5->addWidget(IPLineEdit);


        verticalLayout_3->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        User_label = new QLabel(layoutWidget);
        User_label->setObjectName(QString::fromUtf8("User_label"));

        horizontalLayout_6->addWidget(User_label);

        userLineEdit = new QLineEdit(layoutWidget);
        userLineEdit->setObjectName(QString::fromUtf8("userLineEdit"));
        userLineEdit->setMinimumSize(QSize(190, 25));
        userLineEdit->setMaximumSize(QSize(190, 25));

        horizontalLayout_6->addWidget(userLineEdit);


        verticalLayout_3->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        Port_label_2 = new QLabel(layoutWidget);
        Port_label_2->setObjectName(QString::fromUtf8("Port_label_2"));

        horizontalLayout_7->addWidget(Port_label_2);

        portLineEdit = new QLineEdit(layoutWidget);
        portLineEdit->setObjectName(QString::fromUtf8("portLineEdit"));
        portLineEdit->setMinimumSize(QSize(190, 25));
        portLineEdit->setMaximumSize(QSize(190, 25));

        horizontalLayout_7->addWidget(portLineEdit);


        verticalLayout_3->addLayout(horizontalLayout_7);

        layoutWidget_2 = new QWidget(parametersDialog);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(270, 10, 261, 127));
        verticalLayout_4 = new QVBoxLayout(layoutWidget_2);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(layoutWidget_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font);

        verticalLayout_4->addWidget(label_7);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        label_5 = new QLabel(layoutWidget_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_11->addWidget(label_5);

        baudrateLineEdit = new QLineEdit(layoutWidget_2);
        baudrateLineEdit->setObjectName(QString::fromUtf8("baudrateLineEdit"));
        baudrateLineEdit->setMinimumSize(QSize(170, 25));
        baudrateLineEdit->setMaximumSize(QSize(170, 25));

        horizontalLayout_11->addWidget(baudrateLineEdit);


        verticalLayout_4->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_6 = new QLabel(layoutWidget_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_12->addWidget(label_6);

        serialPortLineEdit = new QLineEdit(layoutWidget_2);
        serialPortLineEdit->setObjectName(QString::fromUtf8("serialPortLineEdit"));
        serialPortLineEdit->setMinimumSize(QSize(170, 25));
        serialPortLineEdit->setMaximumSize(QSize(170, 25));

        horizontalLayout_12->addWidget(serialPortLineEdit);


        verticalLayout_4->addLayout(horizontalLayout_12);

        profilesBox = new QComboBox(parametersDialog);
        profilesBox->setObjectName(QString::fromUtf8("profilesBox"));
        profilesBox->setGeometry(QRect(10, 240, 171, 25));
        layoutWidget1 = new QWidget(parametersDialog);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(190, 190, 138, 120));
        verticalLayout = new QVBoxLayout(layoutWidget1);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        LoadProfileButton = new QPushButton(layoutWidget1);
        LoadProfileButton->setObjectName(QString::fromUtf8("LoadProfileButton"));
        QIcon icon;
        icon.addFile(QString::fromUtf8("images/profile.png"), QSize(), QIcon::Normal, QIcon::Off);
        LoadProfileButton->setIcon(icon);

        verticalLayout->addWidget(LoadProfileButton);

        saveProfileButton = new QPushButton(layoutWidget1);
        saveProfileButton->setObjectName(QString::fromUtf8("saveProfileButton"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("images/saveProfile.png"), QSize(), QIcon::Normal, QIcon::Off);
        saveProfileButton->setIcon(icon1);

        verticalLayout->addWidget(saveProfileButton);

        removeProfileButton = new QPushButton(layoutWidget1);
        removeProfileButton->setObjectName(QString::fromUtf8("removeProfileButton"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8("images/deleteUser.png"), QSize(), QIcon::Normal, QIcon::Off);
        removeProfileButton->setIcon(icon2);

        verticalLayout->addWidget(removeProfileButton);

        saveAsDefaultButton = new QPushButton(layoutWidget1);
        saveAsDefaultButton->setObjectName(QString::fromUtf8("saveAsDefaultButton"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8("images/defaultUser.png"), QSize(), QIcon::Normal, QIcon::Off);
        saveAsDefaultButton->setIcon(icon3);

        verticalLayout->addWidget(saveAsDefaultButton);


        retranslateUi(parametersDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), parametersDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), parametersDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(parametersDialog);
    } // setupUi

    void retranslateUi(QDialog *parametersDialog)
    {
        parametersDialog->setWindowTitle(QApplication::translate("parametersDialog", "Settings", nullptr));
        label_8->setText(QApplication::translate("parametersDialog", "SSH Parameters", nullptr));
        IP_label->setText(QApplication::translate("parametersDialog", "IP :", nullptr));
        IPLineEdit->setText(QString());
        User_label->setText(QApplication::translate("parametersDialog", "User :", nullptr));
        userLineEdit->setText(QString());
        Port_label_2->setText(QApplication::translate("parametersDialog", "Port :", nullptr));
        portLineEdit->setText(QString());
        label_7->setText(QApplication::translate("parametersDialog", "TTY Parameters", nullptr));
        label_5->setText(QApplication::translate("parametersDialog", "Baudrate :", nullptr));
        baudrateLineEdit->setText(QString());
        label_6->setText(QApplication::translate("parametersDialog", "Serial Port :", nullptr));
        serialPortLineEdit->setText(QString());
        LoadProfileButton->setText(QApplication::translate("parametersDialog", "Load Profile", nullptr));
        saveProfileButton->setText(QApplication::translate("parametersDialog", "Save Profile", nullptr));
        removeProfileButton->setText(QApplication::translate("parametersDialog", "Remove Profile", nullptr));
        saveAsDefaultButton->setText(QApplication::translate("parametersDialog", "Save as Default", nullptr));
    } // retranslateUi

};

namespace Ui {
    class parametersDialog: public Ui_parametersDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PARAMETERSDIALOG_H
