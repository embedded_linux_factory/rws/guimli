#ifndef CUSTOMTREEVIEW_H
#define CUSTOMTREEVIEW_H

#include <QTreeView>
#include "myfilesysmodel.h"
extern MyFileSysModel * ssh_model;
extern MyFileSysModel * m_model;


class CustomTreeView : public QTreeView
{
    Q_OBJECT
public:
    CustomTreeView(QWidget *parent);


protected:

    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
};

#endif // CUSTOMTREEVIEW_H
