#ifndef MYFILESYSMODEL_H
#define MYFILESYSMODEL_H

#include <QWidget>
#include <QFileSystemModel>

class MyFileSysModel : public QFileSystemModel
{
    Q_OBJECT
public:
    MyFileSysModel();
    explicit MyFileSysModel(QWidget *parent = nullptr);

protected :
    Qt::ItemFlags flags(const QModelIndex &index) const;
    Qt::DropActions supportedDropActions() const;

};

#endif // MYFILESYSMODEL_H
