#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemModel>
#include "qsshsocket.h"
#include "editorview.h"
#include "myfilesysmodel.h"
#include "customtreeview.h"
#include <QFileSystemWatcher>
#include <QListWidgetItem>

#include <QResizeEvent>
#include "socketinterface.h"

QT_BEGIN_NAMESPACE
class QAction;
class QMenu;
QT_END_NAMESPACE


extern MyFileSysModel * ssh_model;
extern MyFileSysModel * m_model;
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();



private slots:




    void sshConnect();
    void sshDisconnect();


    //MENU ACTIONS
    void ttyConnect();
    void removeCurrentTab();
    void on_refreshButton_clicked();
    void readDefaultProfile();
    void openParamDialog();


    void on_TerminalTabWidget_tabBarClicked(int index);

    void on_ChipsetList_itemDoubleClicked(QListWidgetItem *item);

    void resizeEvent(QResizeEvent *event);

    void on_reservePinButton_clicked();

    void on_releasePinButton_clicked();

    void on_on_off_pushButton_clicked();




private:
    //Menu Functions
    void createActions();
    void createStatusBar();




    Ui::MainWindow *ui;
    QFileSystemWatcher watcher;
    QString working_path;
    bool  is_on;
    bool ssh_connected;
    std::vector<std::thread> socketThreads;
    std::vector<std::string> pathsTemp;
    std::map<std::string, QSshSocket *> sockets;
    std::map<std::string, SocketInterface *>  socketsInterfaces;
    SocketInterface * current_socket;
    CustomTreeView * SSH_tree_view;

    QString ip;
    QString user;
    QString port;
    QString baudrate;
    QString serialPort;


};
#endif // MAINWINDOW_H
