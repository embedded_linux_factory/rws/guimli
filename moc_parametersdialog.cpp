/****************************************************************************
** Meta object code from reading C++ file 'parametersdialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.8)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "parametersdialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'parametersdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.8. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_parametersDialog_t {
    QByteArrayData data[12];
    char stringdata0[206];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_parametersDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_parametersDialog_t qt_meta_stringdata_parametersDialog = {
    {
QT_MOC_LITERAL(0, 0, 16), // "parametersDialog"
QT_MOC_LITERAL(1, 17, 6), // "accept"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 11), // "saveProfile"
QT_MOC_LITERAL(4, 37, 11), // "readProfile"
QT_MOC_LITERAL(5, 49, 11), // "std::string"
QT_MOC_LITERAL(6, 61, 8), // "username"
QT_MOC_LITERAL(7, 70, 15), // "checkParameters"
QT_MOC_LITERAL(8, 86, 28), // "on_LoadProfileButton_clicked"
QT_MOC_LITERAL(9, 115, 30), // "on_removeProfileButton_clicked"
QT_MOC_LITERAL(10, 146, 28), // "on_saveProfileButton_clicked"
QT_MOC_LITERAL(11, 175, 30) // "on_saveAsDefaultButton_clicked"

    },
    "parametersDialog\0accept\0\0saveProfile\0"
    "readProfile\0std::string\0username\0"
    "checkParameters\0on_LoadProfileButton_clicked\0"
    "on_removeProfileButton_clicked\0"
    "on_saveProfileButton_clicked\0"
    "on_saveAsDefaultButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_parametersDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x08 /* Private */,
       3,    0,   55,    2, 0x08 /* Private */,
       4,    1,   56,    2, 0x08 /* Private */,
       7,    0,   59,    2, 0x08 /* Private */,
       8,    0,   60,    2, 0x08 /* Private */,
       9,    0,   61,    2, 0x08 /* Private */,
      10,    0,   62,    2, 0x08 /* Private */,
      11,    0,   63,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Bool,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void parametersDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<parametersDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->accept(); break;
        case 1: _t->saveProfile(); break;
        case 2: _t->readProfile((*reinterpret_cast< std::string(*)>(_a[1]))); break;
        case 3: { bool _r = _t->checkParameters();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 4: _t->on_LoadProfileButton_clicked(); break;
        case 5: _t->on_removeProfileButton_clicked(); break;
        case 6: _t->on_saveProfileButton_clicked(); break;
        case 7: _t->on_saveAsDefaultButton_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject parametersDialog::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_parametersDialog.data,
    qt_meta_data_parametersDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *parametersDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *parametersDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_parametersDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int parametersDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
