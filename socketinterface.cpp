#include "socketinterface.h"
#include "communicationinterface.h"

#include <nlohmann/json.hpp>
using json = nlohmann::json;
#include <QString>
#include <QTreeWidget>

SocketInterface::SocketInterface(std::string ip,QListWidget* chipsets)
{
    ChipsetList = chipsets;

    std::cout << "IP : " << ip << std::endl;
     char const * hello = "Hello from client";


     //TODO Gestion d'errerurs à la place des return -1
     if ((socket = ::socket(AF_INET, SOCK_STREAM, 0)) < 0) {
         printf("\n Socket creation error \n");
         //return -1;
     }

     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = inet_addr(ip.c_str());
     serv_addr.sin_port = htons(PORT_SOCKET);

     // Convert IPv4 and IPv6 addresses from text to binary
     // form
     if (inet_pton(AF_INET, ip.c_str(), &serv_addr.sin_addr)
         <= 0) {
         printf(
             "\nInvalid address/ Address not supported \n");
         //return -1;
     }

     if (::connect(socket, (struct sockaddr*)&serv_addr,sizeof(serv_addr))< 0) {
         printf("\nConnection Failed \n");
         //return -1;
     }
     sendMsg(hello);
     printf("Hello message sent\n");

     stopSignal = false;






}

SocketInterface::~SocketInterface(){

}

void SocketInterface::receive(){

    //TODO rajouter un timeout pour terminer le thread
    char const * disconnectMsg = "Disconnecting";
    int ret;
    while(1){
        if(stopSignal){
            break;
        }

        char buffer[MAX_BUFFER_SIZE] = { 0 };
        if((ret = read(socket, buffer, sizeof(buffer)-1)) > 0){
            buffer[ret] = 0x00;
            printf("block read: \n<%s>\n", buffer);
        }
        if (strncmp(buffer,"Disconnect",sizeof(buffer))==0){
            printf("Disconnect ");
            send(socket, disconnectMsg, strlen(disconnectMsg), 0);
            sendMsg(disconnectMsg);
            break;
        }
        else if(strncmp(buffer,"",sizeof(buffer))!=0){

            printf("%s\n", buffer);
            printf("%d \n", strncmp(buffer,"Disconnect",sizeof(buffer)));
            std::cout <<"strncmp(buffer,\"Disconnect\",sizeof(buffer))" <<  strncmp(buffer,"Disconnect",sizeof(buffer)) << std::endl;
            decodeChipset(buffer);

        }



    }

}

void SocketInterface::stopThread(){
    stopSignal = true;
}


void SocketInterface::decodeChipset(const char * message){
    json msg = json::parse(message);

    std::string s = msg.dump();

    std::cout << msg << std::endl;

    std::cout << msg.dump(5) << std::endl;

    if( msg.size() !=0){
        for (const auto& item : msg.items())
        {
           std::cout << "name :" << item.key() << "\n";

           //std::cout << "value : " << item.value() << std::endl;
           //std::cout << "offset : " << item.value()["of"].get<int>() << std::endl;
           Chipset * chipset = new Chipset(item.key());
           ChipsetList->addItem(QString::fromStdString(item.key()));


           for (const auto& val : item.value().items())
           {

               std::cout << "value : " << val.value() << std::endl;
               std::cout << "offset : " << val.value()["of"].get<int>() << std::endl;
               int offset = val.value()["of"].get<int>();

               int direction = val.value()["d"].get<int>();
               int value = val.value()["v"].get<int>();
               int used = val.value()["u"].get<int>();
               std::string user = val.value()["us"].get<std::string>();
               Pin * new_pin = new Pin(offset, direction, value,used, user);
               chipset->addPin(new_pin);
           }
        chipset_map.insert(std::pair<std::string,Chipset*>(item.key(),chipset));
        }
    } else {
        //TODO gestion erreur
    }

}

void SocketInterface::finishedListening(){
    std::cout << "thread terminated closing connection" << std::endl;
}

void SocketInterface::sendMsg(char const * buffer){
     send(socket, buffer, strlen(buffer), 0);
}

void SocketInterface::sendOn(){
    char const * msg = "sending on";
    sendMsg(msg);
}


void SocketInterface::sendOff(){
    char const * msg = "sending off";
    sendMsg(msg);
}


void SocketInterface::reserve(Chipset * chipset,Pin * pin, int value){
    pin->used = value;
    std::cout << "reserve with value : " << value << std::endl;
    chipset->encodeToJson();
}

void SocketInterface::release(Chipset * chipset,Pin * pin){
    reserve(chipset,pin,0);
}

void SocketInterface::changeValue(Chipset * chipset,Pin * pin, int value){

}

void SocketInterface::showChipset(){
    for (auto chipset : getChipset()){
        ChipsetList->addItem(QString::fromStdString(chipset.first));
    }
}



