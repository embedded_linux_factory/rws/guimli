#ifndef PARAMETERSDIALOG_H
#define PARAMETERSDIALOG_H

#include <QDialog>

namespace Ui {
class parametersDialog;
}

class parametersDialog : public QDialog
{
    Q_OBJECT

public:
    parametersDialog(QWidget *parent,
                                QString * ip,
                                QString * user,
                                QString * port,
                                QString * baudrate,
                                QString * serialPort);
    ~parametersDialog();

private slots:

     /*!
        \brief called when accept signal emited.
    */
    void accept();

    /*!
        \brief Save the profile given by the user in the json format under the "Profiles" folder.
    */
    void saveProfile();


    /*!
        \brief Read the given profile and update the view.
    */
    void readProfile(std::string username);

    /*!
        \brief Check if all the parameters are given to make a profile.
    */
    bool checkParameters();


    void on_LoadProfileButton_clicked();

    void on_removeProfileButton_clicked();

    void on_saveProfileButton_clicked();

    void on_saveAsDefaultButton_clicked();

private:
    Ui::parametersDialog *ui;
    QString * ip;
    QString * user;
    QString * port;
    QString * baudrate;
    QString * serialPort;
};

#endif // PARAMETERSDIALOG_H
