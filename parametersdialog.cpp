#include "parametersdialog.h"
#include "ui_parametersdialog.h"

#include "iostream"
#include <QMessageBox>
#include <nlohmann/json.hpp>
#include <QDir>
#include <fstream>
// for convenience
using json = nlohmann::json;


parametersDialog::parametersDialog(QWidget *parent ,
                                   QString * ip,
                                   QString * user,
                                   QString * port,
                                   QString * baudrate,
                                   QString * serialPort) :
    QDialog(parent),
    ui(new Ui::parametersDialog)
{
    ui->setupUi(this);
    this->ip = ip;
    this->user = user;
    this->port = port;
    this->baudrate = baudrate;
    this->serialPort = serialPort;



    this->ui->IPLineEdit->setText( (*ip));
    this->ui->userLineEdit->setText(  (*user));
    this->ui->portLineEdit->setText( (*port));
    this->ui->baudrateLineEdit->setText(  (*baudrate));
    this->ui->serialPortLineEdit->setText( (*serialPort));




    QDir profiles("Profiles");
    if (!profiles.exists())
    {
        std::cerr << "the Profiles directory should have been created but is missing" << std::endl;
    }
    else{
        foreach( const QFileInfo& entry, profiles.entryInfoList( QStringList() << "*.json", QDir::Files ) ) {

          if(entry.baseName() != "default"){
            this->ui->profilesBox->addItem(entry.baseName());
          }
        }

    }
}

parametersDialog::~parametersDialog()
{
    delete ui;
}


void parametersDialog::accept(){

    if(checkParameters()){

        *ip = this->ui->IPLineEdit->text();
        *user = this->ui->userLineEdit->text();
        *port = this->ui->portLineEdit->text();
        *baudrate = this->ui->baudrateLineEdit->text();
        *serialPort = this->ui->serialPortLineEdit->text();

        emit accepted();
        this->close();

    }

}


void parametersDialog::saveProfile() {
    json jsonfile ;

    //TODO rajouter les verif si les paramètres sont nuls
    jsonfile["ssh"] = { {"ip", this->ui->IPLineEdit->text().toStdString()}, {"user", this->ui->userLineEdit->text().toStdString()}, {"port", this->ui->portLineEdit->text().toInt()} };

    jsonfile["tty"] = { {"baudrates", this->ui->baudrateLineEdit->text().toInt()}, {"serial", this->ui->serialPortLineEdit->text().toStdString()} };

    std::string s = jsonfile.dump();

    std::cout << jsonfile.dump(4) << std::endl;
    std::ofstream file("Profiles/"+this->ui->userLineEdit->text().toStdString()+".json");
    file << jsonfile.dump(4);
    this->ui->profilesBox->addItem(this->ui->userLineEdit->text());
}


void parametersDialog::on_LoadProfileButton_clicked()
{
    std::string username = this->ui->profilesBox->currentText().toStdString();
    readProfile(username);
}


void parametersDialog::readProfile(std::string username){

    std::cout << username << std::endl;

    std::ifstream ifs;
    ifs.open ("Profiles/"+username + ".json");

    json jf = json::parse(ifs);



    std::cout << jf << std::endl;
    std::cout << "ip : " << jf["ssh"]["ip"] << std::endl;

    int port = jf["ssh"]["port"];
    int baudrates = jf["tty"]["baudrates"];

    this->ui->IPLineEdit->setText(QString::fromStdString(jf["ssh"]["ip"] ));
    this->ui->userLineEdit->setText(QString::fromStdString(jf["ssh"]["user"]));
    this->ui->portLineEdit->setText(QString::number(port));

    this->ui->baudrateLineEdit->setText(QString::number(baudrates));
    this->ui->serialPortLineEdit->setText(QString::fromStdString(jf["tty"]["serial"]));
    ifs.close();

}

void parametersDialog::on_removeProfileButton_clicked()
{
    std::string username = this->ui->profilesBox->currentText().toStdString();
    this->ui->profilesBox->removeItem(this->ui->profilesBox->currentIndex());
    std::string command  = "rm Profiles/"+username+".json";
    if ( system(command.c_str()) == -1 ){
        std::cerr << "Error when deleting profile : " << username << std::endl;
    }
}

bool parametersDialog::checkParameters(){
    QString invalid_parameters= "";
    if (this->ui->IPLineEdit->text() == "" || this->ui->IPLineEdit->text().count(".") != 3){ //TODO verification sur la validité de l'ip
        invalid_parameters += "IP ";

    }
    if (this->ui->userLineEdit->text() == ""){
        invalid_parameters += " user ";

    }
    if (this->ui->portLineEdit->text() == ""){
        //TODO verifier que c'est un chiffre et qu'il est compris dans la plage.
        invalid_parameters += " port ";

    }
    if (this->ui->baudrateLineEdit->text() == ""){
        //TODO verifier que le baudrate est existant
        invalid_parameters += " baudrate ";

    }
    if (this->ui->serialPortLineEdit->text() == ""){
        invalid_parameters += " serial port ";

    }
    if( invalid_parameters == ""){

       return true;
    }else {
        QMessageBox msgBox;
        msgBox.setText("The following parameters  : " + invalid_parameters + " are invalid");
        msgBox.exec();

    }
    return false;
}
void parametersDialog::on_saveProfileButton_clicked()
{
    if (checkParameters()){
         saveProfile();
    }



}

void parametersDialog::on_saveAsDefaultButton_clicked()
{
    if (checkParameters()){
        json jsonfile ;

        //TODO rajouter les verif si les paramètres sont nuls
        jsonfile["ssh"] = { {"ip", this->ui->IPLineEdit->text().toStdString()}, {"user", this->ui->userLineEdit->text().toStdString()}, {"port", this->ui->portLineEdit->text().toInt()} };

        jsonfile["tty"] = { {"baudrates", this->ui->baudrateLineEdit->text().toInt()}, {"serial", this->ui->serialPortLineEdit->text().toStdString()} };

        std::string s = jsonfile.dump();

        std::cout << jsonfile.dump(4) << std::endl;
        std::ofstream file("Profiles/default.json");
        file << jsonfile.dump(4);
        this->ui->profilesBox->addItem(this->ui->userLineEdit->text());
    }
}
