#ifndef CHIPSET_H
#define CHIPSET_H

#include <string>
#include "pin.h"
#include <QTreeWidget>

#include <nlohmann/json.hpp>
using json = nlohmann::json;


class Chipset
{
public:
    Chipset(std::string chipset_name);


    void addPin(Pin * pin);
    std::string getName(){return name;}
    std::vector<Pin *> getPins(){return pins;}
    void showPins(QTreeWidget * tree);
    json encodeToJson();

private:
   std::string name;
   std::vector<Pin *> pins;
};

#endif // CHIPSET_H
