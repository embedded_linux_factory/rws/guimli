#include "customtreeview.h"
#include <QDropEvent>
#include <QDragEnterEvent>
#include <QMimeData>
#include <QFileInfo>
#include <iostream>
#include "mainwindow.h"

#include <QErrorMessage>

CustomTreeView::CustomTreeView(QWidget *parent) : QTreeView(parent)
{
    setFixedWidth(600);
    setFixedHeight(600);

}


void CustomTreeView::dragEnterEvent(QDragEnterEvent *event)
{
    std::cout << "drag enter event" << std::endl;
    foreach(QUrl url, event->mimeData()->urls())
    {
        std::cout << "type of the file " << (QFileInfo(url.toLocalFile()).suffix()).toStdString() << std::endl;
        std::cout << "url " << url.toString().toStdString() << std::endl;
        event->acceptProposedAction();
        return;

    }
}
void CustomTreeView::dropEvent(QDropEvent *event)
{
    std::cout << "drop event" << std::endl;
    foreach(QUrl url, event->mimeData()->urls())
    {

        QString source_path = url.toLocalFile();

        QString suffix = QFileInfo(source_path).suffix().toUpper();
        QPoint p = parentWidget()->mapFromGlobal(QCursor::pos());
        QPoint corrected_point = QPoint(p.rx(), p.ry()-20); // there seems to be a wrong reading of the correct y position.
        QString dest_path = ssh_model->filePath(indexAt(corrected_point));

        //if the dropped item is a Dir
        if( QFileInfo(source_path).isDir()){
            std::string command  = "cp -r " + source_path.toStdString()  + " " + dest_path.toStdString();
            if ( system(command.c_str()) == -1){
                QErrorMessage * errorMessage =  new QErrorMessage(parentWidget());
                errorMessage->showMessage("Unexpected error when copying your directory");
                std::cerr << "error while cp directory with command " << command << std::endl;
            }


        //if the dropped item is a file
        } else if (QFileInfo(source_path).isFile()){
            std::string command  = "cp " + source_path.toStdString()  + " " + dest_path.toStdString();
            if ( system(command.c_str()) == -1 ){
                QErrorMessage * errorMessage =  new QErrorMessage(parentWidget());
                errorMessage->showMessage("Unexpected error when copying your file");
                std::cerr << "error while cp directory with command " << command << std::endl;
            }


        }

    }
}
