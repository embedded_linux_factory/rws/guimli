#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QProcess>
#include <iostream>
#include <QStringList>
#include <QDir>
#include <QTreeView>
#include <QTextEdit>
#include "customtreeview.h"
#include <string>
#include <QDirIterator>
#include <fstream>
#include "parametersdialog.h"
#include <nlohmann/json.hpp>
#include <QMessageBox>

//Socket
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>

#define PORT 8080

// for convenience
using json = nlohmann::json;

using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ssh_connected = false;
    is_on =false;
    SSH_tree_view = new CustomTreeView( this->ui->TreeViewWidget);


    //Creating menu
    createActions();

    //Check for existing Profiles
    QDir profiles("Profiles");
    if (!profiles.exists())
    {
        if ( system("mkdir Profiles") == -1){
            std::cerr << "error when creating the directory for the profiles" << std::endl;
        }
    }
    else{
        readDefaultProfile();

    }



}

MainWindow::~MainWindow()
{
    for (auto socket : socketsInterfaces){
        socket.second->stopThread();
    }

    for (std::thread & th : socketThreads)
    {
        //if the receiving socket thread is still active wait for closure.
        //TODO : En réalité ici il faut forcer la fermeture du thread et ne pas forcément attendre de retour du serveur.
        if (th.joinable())
            th.join();
    }

    if (ssh_connected){
        for (auto path :pathsTemp ){
            std::string command = "fusermount -u "+  path;
             if ( system(command.c_str()) == -1 ){
                std::cout << "error during unmount with error " << errno << std::endl;
            }
            command = "rmdir " + path;
            if ( system(command.c_str()) == -1){

                 std::cout << "Error : failed to remove tmp dir, returned error :" << errno << std::endl;
            }
        }
        for (auto const& it : sockets ){
            it.second->disconnectFromHost();
            sockets.erase(it.first);
        }


    }
    delete ui;
}


void MainWindow::createActions()
{

    //Creating the menu and the ToolBar
    QMenu * SSH_menu = menuBar()->addMenu(tr("&Parameters"));
    QToolBar * sshToolBar = addToolBar(tr("File"));


    //SSH Connection Action
    QAction * connectAct = new QAction( QIcon(QDir::currentPath() + "/images/connection.png"),tr("&Connect"), this);
    connectAct->setShortcut(QKeySequence(tr("Ctrl+H")));
    connectAct->setStatusTip(tr("Connect with ssh and open a terminal"));
    connect(connectAct, &QAction::triggered, this, &MainWindow::sshConnect); //TODO A bien evidemment changer
    SSH_menu->addAction(connectAct);
    sshToolBar->addAction(connectAct);



    //Settings Action
    QAction * settingsAct = new QAction( QIcon(QDir::currentPath() + "/images/settings.png") , tr("&Settings"), this);
    settingsAct->setShortcuts(QKeySequence::New);
    settingsAct->setStatusTip(tr("open SSH setting"));
    connect(settingsAct, &QAction::triggered, this, &MainWindow::openParamDialog); //TODO A bien evidemment changer
    SSH_menu->addAction(settingsAct);
    sshToolBar->addAction(settingsAct);


    //TTY Connection Action
    QAction * openNewTermAct = new QAction( QIcon(QDir::currentPath() + "/images/terminal.png"), tr("&TTY connection"), this);
    openNewTermAct->setShortcuts(QKeySequence::Open);
    openNewTermAct->setStatusTip(tr("Open new Terminal with on TTY selected"));
    connect(openNewTermAct, &QAction::triggered, this, &MainWindow::ttyConnect); //TODO A bien evidemment changer
    SSH_menu->addAction(openNewTermAct);
    sshToolBar->addAction(openNewTermAct);


    //Disconnect Action
    QAction * disconnectAct = new QAction( QIcon(QDir::currentPath() + "/images/disconnect.png"), tr("&disconnect"), this);
    disconnectAct->setShortcuts(QKeySequence::Close);
    disconnectAct->setStatusTip(tr("disconnect from the SSH connection and close all related terminals"));
    connect(disconnectAct, &QAction::triggered, this, &MainWindow::sshDisconnect); //TODO créer la fonction disconnect associé
    SSH_menu->addAction(disconnectAct);
    sshToolBar->addAction(disconnectAct);
    //SSH_menu->addAction(test_menu);


    //Close Action
    QAction * closeCurrentTermAct = new QAction( QIcon(QDir::currentPath() + "/images/closeTerminal.png"), tr("&close"), this);
    closeCurrentTermAct->setShortcut(QKeySequence(tr("Ctrl+D")));
    openNewTermAct->setStatusTip(tr("close current terminal"));
    connect(closeCurrentTermAct, &QAction::triggered, this, &MainWindow::removeCurrentTab); //TODO A bien evidemment changer
    SSH_menu->addAction(closeCurrentTermAct);
    sshToolBar->addAction(closeCurrentTermAct);




}

void MainWindow::openParamDialog(){
    parametersDialog * paramWindows = new parametersDialog(this,&ip,&user,&port,&baudrate,&serialPort);
    paramWindows->show();

}







void MainWindow::readDefaultProfile(){


    std::string username = "default";
    std::ifstream ifs;
    //open the associated json profile
    ifs.open ("Profiles/"+username + ".json"); 
    json jf = json::parse(ifs);

    //std::cout << jf << std::endl;
    //std::cout << "ip : " << jf["ssh"]["ip"] << std::endl;

    int portDefault = jf["ssh"]["port"];
    int baudratesDefault = jf["tty"]["baudrates"];

    ip = QString::fromStdString(jf["ssh"]["ip"] );
    user = QString::fromStdString(jf["ssh"]["user"]);
    port = QString::number(portDefault);

    baudrate = QString::number(baudratesDefault);
    serialPort = QString::fromStdString(jf["tty"]["serial"]);
    ifs.close();

}


void MainWindow::sshConnect(){
    //Checking if the current parameters are given for the ssh connection
    if (user != "" && ip != "" && port != ""){

        working_path  = QDir::currentPath() + QString("/tmp");
        QString ip_tmp = ip;
        ip_tmp.replace(".","_");

         //Check if we have already done the connection in SSH
         bool already_added = false;
         for (int i = 0; i < this->ui->TerminalTabWidget->count(); i ++){
             if (this->ui->TerminalTabWidget->tabText(i) == "Terminal : " + ip){
                 already_added = true;
             }
             std::cout << "tab name  : " << this->ui->TerminalTabWidget->tabText(i).toStdString() << std::endl;
         }

         if(!already_added){


             //Creating the Socket Interface used to dialog in socket
             SocketInterface * socket = new SocketInterface(ip.toStdString(), this->ui->ChipsetList);
             socketsInterfaces.insert(std::pair<std::string, SocketInterface *>(ip.toStdString(),socket));

             //Launching the receive Thread , TODO : à lancer si possible lors de la création de la SocketInterface
             std::thread socketThread (&SocketInterface::receive,socket);
             socketThreads.push_back(std::move(socketThread));
             current_socket = socket;

             //Creating the QSshSocket
             QSshSocket *  ssh_socket= new QSshSocket();

             //Giving the current parameters to the  QSshSocket
             ssh_socket->connectToHost(ip,port.toInt());
             ssh_socket->login(user,"");

             sockets[ip.toStdString()] =ssh_socket;


             std::cout << " NOT ALREADY ADDED" << " this->ui->TerminalTabWidget->count() : " << this->ui->TerminalTabWidget->count()<<  std::endl;
             if (this->ui->TerminalTabWidget->tabText(0) == "Terminal"){
                 this->ui->TerminalTabWidget->setTabText(0,ip);
             } else {
                 QWidget * newWidget = new QWidget();
                 this->ui->TerminalTabWidget->addTab( newWidget, ip);
             }


             //if ssh connexion failed we stop the attempt to connect to the remote host
             if (!ssh_socket->isConnected()){
                 //TODO gestion erreur socket non connecté.
                 return;
             }


          }

    //Launching the gnome terminal in our GUI
     QProcess *child = new QProcess(this->ui->TerminalTabWidget->widget(this->ui->TerminalTabWidget->count()-1));
     int id  = this->ui->TerminalTabWidget->widget(this->ui->TerminalTabWidget->count()-1)->winId();
     std::cout << "win ID : " << id  << std::endl;
     QString   cmd   = "xterm -hold  -geometry 300x300 -into " + QString::number(id) +" -e ssh " + user + "@" + ip +" -p" + port +" " ;
     child->start(cmd);


     //Creating the associated directory used for sshfs
      std::string pathTmp = QDir::currentPath().toStdString()+"/tmp_" + ip_tmp.toStdString();
      pathsTemp.push_back(pathTmp);
      std::cout << "pathTmp : " << pathTmp << std::endl;
      std::string command = "mkdir "+pathTmp; //TODO vérifier qu'il n'est pas déja crée et actuellement monté.
      if ( system(command.c_str()) == -1){
           std::cout << "Error : failed to create tmp dir, returned error :" << errno << std::endl;
      }

      //SSHFS the remote host home directory
      command=  "sshfs " + user.toStdString() + "@" + ip.toStdString() + ":/home tmp_"+ ip_tmp.toStdString();
      if ( system(command.c_str()) == -1 ){
         std::cout << "error with command " << command << " with error " << errno << std::endl;

      }else{

          //creating the model corresponding to the tree view of the remote /home
          ssh_model = new MyFileSysModel(SSH_tree_view);
          ssh_model->setRootPath(QString::fromStdString(pathTmp));

          SSH_tree_view->setModel(ssh_model);
          SSH_tree_view->setRootIndex(ssh_model->setRootPath(QString::fromStdString(pathTmp)));
          SSH_tree_view->setSelectionMode(QAbstractItemView::SingleSelection);
          SSH_tree_view->setDragEnabled(true);
          SSH_tree_view->viewport()->setAcceptDrops(true);
          SSH_tree_view->setDropIndicatorShown(true);
          SSH_tree_view->setDragDropMode(QAbstractItemView::InternalMove);
          SSH_tree_view->setAcceptDrops(true);
          SSH_tree_view->hideColumn(1);


          is_on =true;
          ssh_connected = true;

      }


     }
     else {
         if(user == ""){
             QMessageBox msgBox;
             msgBox.setText("No user");
             msgBox.exec();
             //std::cerr << "No user " << std::endl;
         }
         else if(ip == ""){
             QMessageBox msgBox;
             msgBox.setText("No ip");
             msgBox.exec();

         }
         else if(port == ""){
             QMessageBox msgBox;
             msgBox.setText("No port");
             msgBox.exec();

         }



     }
}

void MainWindow::sshDisconnect(){

    //For all Terminals if their ip correspond to the current profile remove them
     for (int i = this->ui->TerminalTabWidget->count()-1; i >= 0; i --){
         QString ip_tab ;
          if (this->ui->TerminalTabWidget->tabText(i).count(":") == 1){ //TTY terminal
              ip_tab = this->ui->TerminalTabWidget->tabText(i).split(QLatin1Char(':')).at(0);
              ip_tab.chop(1); // removing the little space
          }else if (this->ui->TerminalTabWidget->tabText(i).count(":") == 0){ //SSH terminal
              ip_tab = this->ui->TerminalTabWidget->tabText(i);
          }
          if (ip_tab == ip){
               this->ui->TerminalTabWidget->removeTab(i);

          }
     }
     //Disconnect the associated socket
     if(sockets.find(ip.toStdString()) != sockets.end()){
          sockets[ip.toStdString()]->disconnectFromHost();
          sockets.erase(ip.toStdString());
      }
     //Stop the associated SocketInterfaces
     if(socketsInterfaces.find(ip.toStdString()) != socketsInterfaces.end()){
          socketsInterfaces[ip.toStdString()]->stopThread();
     }

}


void MainWindow::on_on_off_pushButton_clicked()
{
    //IN PROGRESS ...
    std::cout << "sending state :" << std::endl;
}


void MainWindow::ttyConnect()
{
    //If TTY settings are ok ...
    if ( baudrate != "" && serialPort != ""){
        bool already_added = false;
        QTabWidget * terminals = this->ui->TerminalTabWidget;
        QTextEdit * newTerm = new QTextEdit();
        //check if the connection is already done
        for (int i = 0; i < terminals->count(); i ++){
            if (terminals->tabText(i) == serialPort){
                already_added = true;
            }
            std::cout << "tab name  : " << terminals->tabText(i).toStdString() << std::endl;
        }

        if (!already_added){


            //Check if we succesfully already connected to ssh connection
            if(sockets.find(ip.toStdString()) != sockets.end()){

                terminals->addTab( newTerm, ip + " : " + serialPort);

                QSshSocket * socket = sockets[ip.toStdString()];
                socket->connectToHost(ip,port.toInt());
                socket->login(user,"");


                if (!socket->isConnected()){
                    //TODO gestion erreur socket non connecté.
                    std::cerr << "SSH Connection failed" << std::endl;
                    return;
                } else {
                    //starting the terminal
                    QProcess *child = new QProcess(newTerm);
                    QString   cmd   = "xterm -hold -geometry 100x100 -into " + QString::number(newTerm->winId()) +" -e ssh " + user + "@"
                            + ip +" -p" + port +" " + " picocom -b "+ baudrate +" -r -l /dev/" + serialPort;
                    child->start(cmd);
                }



            } else {
                std::cout << "please connect first to ssh" << std::endl;
            }
        }
    } else {
        if( baudrate == ""){
            QMessageBox msgBox;
            msgBox.setText("No baudrate");
            msgBox.exec();
            std::cerr << "No baudrate " << std::endl;
        }
        else if( serialPort == ""){
            QMessageBox msgBox;
            msgBox.setText("No serial port");
            msgBox.exec();
            std::cerr << "No serial port " << std::endl;
        }
    }


}

void MainWindow::removeCurrentTab()
{

    if( this->ui->TerminalTabWidget->count() > 1  and this->ui->TerminalTabWidget->currentIndex() != 0){
        QString ip = this->ui->TerminalTabWidget->tabText(this->ui->TerminalTabWidget->currentIndex());
        //
        if(ip.count(".") == 3  && ip.count(":") == 0){
           if(sockets.find(ip.toStdString()) != sockets.end()){
               //TODO : Disconnect also all the others tab in TTY see MainWindow::disconnect()
                sockets[ip.toStdString()]->disconnectFromHost();
                sockets.erase(ip.toStdString());
            }
        }
        this->ui->TerminalTabWidget->removeTab(this->ui->TerminalTabWidget->currentIndex());

    }
}



void MainWindow::on_refreshButton_clicked()
{
    QString rootPath = ssh_model->rootPath();
    std::cout << "refreshing the view " << std::endl;
    ssh_model = new MyFileSysModel(SSH_tree_view);

    SSH_tree_view->setModel(ssh_model);
    SSH_tree_view->setRootIndex(ssh_model->setRootPath(rootPath));

}


void MainWindow::resizeEvent(QResizeEvent *event) {

    //TODO : function unfinished that still bug
    // Possible solution : relaunch the terminal at every resize.

    if (isMaximized() || isMinimized() ) {
        QSize size = this->size();
        std::cout  << "width" << size.width() << std::endl;
        this->ui->TerminalTabWidget->resize(size.width()-490,this->ui->TerminalTabWidget->height());
        for (int i = 0; i < this->ui->TerminalTabWidget->count(); i ++){
             QWidget* pWidget = this->ui->TerminalTabWidget->widget(i);


            std::cout << "class name : " << this->ui->TerminalTabWidget->widget(i)->metaObject()->className() << std::endl;
            std::cout  << "width before " << pWidget->width() << "  length before : "  << pWidget->height() << std::endl;
            pWidget->resize(this->ui->TerminalTabWidget->width(),this->ui->TerminalTabWidget->height());
            std::cout << "resizing widget with width :" << this->ui->TerminalTabWidget->width() <<"   and height : " << this->ui->TerminalTabWidget->height() << std::endl;
        }

    }

}


void MainWindow::on_TerminalTabWidget_tabBarClicked(int index)
{

    if (this->ui->TerminalTabWidget->tabText(index).count(".") == 3){
        SSH_tree_view->hideColumn(1);
        //If it's an ssh connection
        if (this->ui->TerminalTabWidget->tabText(index).count(":") == 1){
            QString  str = this->ui->TerminalTabWidget->tabText(index);

            QString ip_tab = str.split(QLatin1Char(':')).at(0);

            current_socket = socketsInterfaces[ip_tab.toStdString().substr (0, ip_tab.toStdString().size()-1)];
            this->ui->ChipsetList->clear();
            this->ui->ChipsetTreeWidget->clear();
            if (current_socket->getChipset().size()!=0){
                 current_socket->showChipset();
            }

            this->ui->nbPinsBox->clear();
            ip_tab.replace(".","_");
            std::string tmp = "tmp_"+ip_tab.toStdString().substr (0, ip_tab.toStdString().size()-1);
            std::string path  = QDir::currentPath().toStdString()+"/"+tmp;

            //Updating the tree view
            if (QString::fromStdString(path) != ssh_model->rootPath()){
                ssh_model = new MyFileSysModel(SSH_tree_view);
                SSH_tree_view->setModel(ssh_model);
                SSH_tree_view->setRootIndex(ssh_model->setRootPath(QString::fromStdString(path)));
            }

        }
        //If it's a tty connection
        else {


            QString  ip_tab = this->ui->TerminalTabWidget->tabText(index);
            current_socket = socketsInterfaces[ip_tab.toStdString()];
            this->ui->ChipsetList->clear();
            this->ui->ChipsetTreeWidget->clear();
            this->ui->nbPinsBox->clear();
            if (current_socket->getChipset().size()!=0){
                current_socket->showChipset();
           }

            std::string tmp = "tmp_"+ip_tab.replace(".","_").toStdString();
            std::string path  = QDir::currentPath().toStdString()+"/"+tmp;
            //Updating the tree view
            if (QString::fromStdString(path) != ssh_model->rootPath()){
                ssh_model = new MyFileSysModel(SSH_tree_view);
                SSH_tree_view->setModel(ssh_model);
                SSH_tree_view->setRootIndex(ssh_model->setRootPath(QString::fromStdString(path)));
            }
        }

    }else {
        std::cerr << "error in the ip of this tab " << std::endl;
    }


}




void MainWindow::on_ChipsetList_itemDoubleClicked(QListWidgetItem *item)
{
    std::cout << item->text().toStdString() << std::endl;
    this->ui->ChipsetTreeWidget->clear();
     this->ui->nbPinsBox->clear();
    current_socket->getChipset()[item->text().toStdString()]->showPins(this->ui->ChipsetTreeWidget);
    for (size_t i = 0; i < current_socket->getChipset()[item->text().toStdString()]->getPins().size() ; i++){
        this->ui->nbPinsBox->addItem(QString::number(i));
    }

}

void MainWindow::on_reservePinButton_clicked()
{
    if ( this->ui->nbPinsBox->count() != 0){
        Chipset * chipset = current_socket->getChipset()[this->ui->ChipsetList->currentItem()->text().toStdString()];
        current_socket->reserve(chipset,chipset->getPins()[this->ui->nbPinsBox->currentText().toInt()],1);
    }

}

void MainWindow::on_releasePinButton_clicked()
{
    if ( this->ui->nbPinsBox->count() != 0){
        Chipset * chipset = current_socket->getChipset()[this->ui->ChipsetList->currentItem()->text().toStdString()];
        current_socket->release(chipset,chipset->getPins()[this->ui->nbPinsBox->currentText().toInt()]);
    }

}
