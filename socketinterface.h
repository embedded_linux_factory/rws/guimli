#ifndef SOCKETINTERFACE_H
#define SOCKETINTERFACE_H


#include <arpa/inet.h>
#include <iostream>
#include <sys/socket.h>
#include <unistd.h>
#include <string>
#include <QThread>
#include <thread>
#include "communicationinterface.h"
#include <QObject>
#include "QListWidget"
#include <QTreeWidget>
#include "map"
#include "pin.h"
#define PORT_SOCKET 5000
#define MAX_BUFFER_SIZE 65536

class SocketInterface : protected CommunicationInterface
{


public:
   SocketInterface(std::string ip, QListWidget* chipsets);
   ~SocketInterface();
   void sendOn();
   void sendOff();
   void receive();

   /*!
       \brief Reserve the pin of the selected chipset and send a message to the work station to update via the socket connection.
   */
   void reserve(Chipset * chipset,Pin * pin, int value);

   /*!
       \brief Release the pin of the selected chipset and send a message to the work station to update via the socket connection.
   */
   void release(Chipset * chipset,Pin * pin);


   /*!
       \brief Change the value of the pin of the selected chipset and send a message to the work station to update via the socket connection.
   */
   void changeValue(Chipset * chipset, Pin * pin, int value);


   void stopThread();
   std::map<std::string,Chipset*> getChipset(){return chipset_map;}

   /*!
       \brief display the chipset info on the view.
   */
   void showChipset();




signals:
   void finishedListening();

private:



   void decodeChipset(const char * message);
   bool stopSignal;
   void sendMsg(char const * buffer);
   int socket;
   struct sockaddr_in serv_addr;
   char * ip;
   QListWidget * ChipsetList;
   std::map<std::string,Chipset*> chipset_map;

   //std::thread socketThread;
};

#endif // SOCKETINTERFACE_H
