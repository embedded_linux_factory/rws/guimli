/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QPushButton *on_off_pushButton;
    QTabWidget *TerminalTabWidget;
    QWidget *tab_3;
    QWidget *TreeViewWidget;
    QPushButton *refreshButton;
    QListWidget *ChipsetList;
    QTreeWidget *ChipsetTreeWidget;
    QPushButton *reservePinButton;
    QPushButton *releasePinButton;
    QComboBox *nbPinsBox;
    QMenuBar *menubar;
    QStatusBar *statusbar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1281, 995);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        on_off_pushButton = new QPushButton(centralwidget);
        on_off_pushButton->setObjectName(QString::fromUtf8("on_off_pushButton"));
        on_off_pushButton->setGeometry(QRect(250, 20, 89, 25));
        TerminalTabWidget = new QTabWidget(centralwidget);
        TerminalTabWidget->setObjectName(QString::fromUtf8("TerminalTabWidget"));
        TerminalTabWidget->setGeometry(QRect(490, 10, 771, 891));
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        TerminalTabWidget->addTab(tab_3, QString());
        TreeViewWidget = new QWidget(centralwidget);
        TreeViewWidget->setObjectName(QString::fromUtf8("TreeViewWidget"));
        TreeViewWidget->setGeometry(QRect(20, 469, 421, 441));
        refreshButton = new QPushButton(centralwidget);
        refreshButton->setObjectName(QString::fromUtf8("refreshButton"));
        refreshButton->setGeometry(QRect(10, 400, 91, 25));
        ChipsetList = new QListWidget(centralwidget);
        ChipsetList->setObjectName(QString::fromUtf8("ChipsetList"));
        ChipsetList->setGeometry(QRect(10, 80, 80, 300));
        ChipsetList->setMaximumSize(QSize(80, 300));
        ChipsetTreeWidget = new QTreeWidget(centralwidget);
        ChipsetTreeWidget->setObjectName(QString::fromUtf8("ChipsetTreeWidget"));
        ChipsetTreeWidget->setGeometry(QRect(90, 80, 390, 300));
        ChipsetTreeWidget->setMaximumSize(QSize(390, 300));
        ChipsetTreeWidget->header()->setDefaultSectionSize(70);
        reservePinButton = new QPushButton(centralwidget);
        reservePinButton->setObjectName(QString::fromUtf8("reservePinButton"));
        reservePinButton->setGeometry(QRect(10, 10, 89, 25));
        releasePinButton = new QPushButton(centralwidget);
        releasePinButton->setObjectName(QString::fromUtf8("releasePinButton"));
        releasePinButton->setGeometry(QRect(10, 40, 89, 25));
        nbPinsBox = new QComboBox(centralwidget);
        nbPinsBox->setObjectName(QString::fromUtf8("nbPinsBox"));
        nbPinsBox->setGeometry(QRect(120, 20, 86, 25));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1281, 22));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);

        retranslateUi(MainWindow);

        TerminalTabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        on_off_pushButton->setText(QApplication::translate("MainWindow", "on/off", nullptr));
        TerminalTabWidget->setTabText(TerminalTabWidget->indexOf(tab_3), QApplication::translate("MainWindow", "Terminal", nullptr));
        refreshButton->setText(QApplication::translate("MainWindow", "Refresh", nullptr));
        QTreeWidgetItem *___qtreewidgetitem = ChipsetTreeWidget->headerItem();
        ___qtreewidgetitem->setText(4, QApplication::translate("MainWindow", "user", nullptr));
        ___qtreewidgetitem->setText(3, QApplication::translate("MainWindow", "used", nullptr));
        ___qtreewidgetitem->setText(2, QApplication::translate("MainWindow", "value", nullptr));
        ___qtreewidgetitem->setText(1, QApplication::translate("MainWindow", "direction", nullptr));
        ___qtreewidgetitem->setText(0, QApplication::translate("MainWindow", "offset", nullptr));
        reservePinButton->setText(QApplication::translate("MainWindow", "reserve Pin", nullptr));
        releasePinButton->setText(QApplication::translate("MainWindow", "release Pin", nullptr));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
