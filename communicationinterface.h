#ifndef COMMUNICATIONINTERFACE_H
#define COMMUNICATIONINTERFACE_H

#include <map>
#include "chipset.h"

class CommunicationInterface
{
protected:

    /*!
        \brief Sent an "on" message to the work station .
    */
    virtual void sendOn(){}

    /*!
        \brief Sent an "off" message to the work station .
    */
    virtual void sendOff(){}

    /*!
        \brief This function receive and decode all messages coming from the work station.
    */
    virtual void receive(){}

    /*!
        \brief Reserve the pin of the selected chipset and send a message to the work station to update.
    */
    virtual void reserve(Chipset * Chipset,Pin * pin, int value){}


    /*!
        \brief Release the pin of the selected chipset and send a message to the work station to update.
    */
    virtual void release(Chipset * Chipset ,Pin * pin){}

    /*!
        \brief Change the value of the pin of the selected chipset and send a message to the work station to update.
    */
    virtual void changeValue(Chipset * Chipset, Pin * pin,int value){}

private:

    std::map<std::string, Chipset> ChipsetsMap;
};

#endif // COMMUNICATIONINTERFACE_H
